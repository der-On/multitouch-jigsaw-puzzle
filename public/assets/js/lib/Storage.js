import utils from './utils.js';

function Storage(namespace) {
  const storage = {
    namespace: namespace,
    data: {}
  };

  storage.load = function () {
    storage.data = JSON.parse(localStorage.getItem(namespace)) || {};
  };

  storage.persist = utils.throttle(function () {
    localStorage.setItem(namespace, JSON.stringify(storage.data));
  }, 2000);

  storage.setItem = function (key, value) {
    storage.data[key] = value;
    storage.persist();
  };

  storage.getItem = function (key, defaultValue) {
    return (typeof storage.data[key] !== 'undefined')
      ? storage.data[key]
      : defaultValue;
  };

  storage.removeItem = function (key) {
    delete storage.data[key];
    storage.persist();
  };

  storage.load();

  return storage;
}

export default Storage;
