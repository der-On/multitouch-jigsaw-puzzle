function shortCode(length = 10) {
  const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let code = '';

  while (code.length < length) {
    code += chars.charAt(Math.round(Math.random() * chars.length));
  }

  return code;
}

// for generating the jigsaws see
// http://dev.inventables.com/2016/02/26/generating-svg-jigsaw-puzzles.html

// Returns 6 points representing the shape of one edge of a puzzle piece.
// Point coordinates are expressed as percentage distances across the width
// and height of the piece.
function edgeDistributions() {
  const baselineOffsets = {
    xMin: 51,
    xMax: 62,
    yMin: -15,
    yMax: 5
  };

  const upperOffsets = {
    xMin: 20,
    xMax: 30,
    yMin: 20,
    yMax: 44
  };

  const point1 = [0, 0];
  const point2 = [
    _.random(baselineOffsets.xMin, baselineOffsets.xMax),
    _.random(baselineOffsets.yMin, baselineOffsets.yMax)
  ];
  const point3 = [
    _.random(upperOffsets.xMin, upperOffsets.xMax),
    _.random(upperOffsets.yMin, upperOffsets.yMax)
  ];
  const point4 = [
    _.random(100 - upperOffsets.xMax, 100 - upperOffsets.xMin),
    _.random(upperOffsets.yMin, upperOffsets.yMax)
  ];
  const point5 = [
    _.random(100 - baselineOffsets.xMax, 100 - baselineOffsets.xMin),
    _.random(baselineOffsets.yMin, baselineOffsets.yMax)
  ];
  const point6 = [100, 0];

  const sign = Math.random() < 0.5 ? -1 : 1;

  return [point1, point2, point3, point4, point5, point6].map(function(p) {
    return [p[0] / 100, p[1] * sign / 100];
  });
};

// Builds an m + 1 x n matrix of edge shapes. The first and last rows
// are straight edges.
function buildDistributions(m, n) {
  const lineGroups = [];
  let lines = [];
  let i;
  let j;

  for (j = 0; j < n; j++) {
    lines.push([[0, 0], [1,0]]);
  }
  lineGroups.push(lines);

  for (i = 1; i < m; i++) {
    lines = [];
    for (j = 0; j < n; j++) {
      lines.push(edgeDistributions());
    }
    lineGroups.push(lines);
  }

  lines = [];
  for (j = 0; j < n; j++) {
    lines.push([[0, 0], [1,0]]);
  }
  lineGroups.push(lines);

  return lineGroups;
};

function transposePoint(point) {
  return [point[1], point[0]];
};

function offsetPoint(x, y, point) {
  return [point[0] + x, point[1] + y];
};

function offsetPoints(lineGroups, offsetter) {
  for (let i = 0; i < lineGroups.length; i++) {
    const lines = lineGroups[i];
    for (let j = 0; j < lines.length; j++) {
      lines[j] = lines[j].map(function(point) {
        return offsetter(point, j, i);
      });
    }
  }
};

function clipPath(edges, scale = 1) {
  return edges.map((edge, index) => {
    if (index === 0) {
      return d3CurvedLine(edge);
    } else {
      return d3CurvedLine(edge).slice(1);
    }
  }).join('\n');
}

const d3CurvedLine = d3_shape.line().curve(d3_shape.curveBasis);

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

// Returns a function, that, when invoked, will only be triggered at most once
// during a given window of time. Normally, the throttled function will run
// as much as it can, without ever going more than once per `wait` duration;
// but if you'd like to disable the execution on the leading edge, pass
// `{leading: false}`. To disable execution on the trailing edge, ditto.
function throttle(func, wait, options) {
  var context, args, result;
  var timeout = null;
  var previous = 0;
  if (!options) options = {};
  var later = function() {
    previous = options.leading === false ? 0 : Date.now();
    timeout = null;
    result = func.apply(context, args);
    if (!timeout) context = args = null;
  };
  return function() {
    var now = Date.now();
    if (!previous && options.leading === false) previous = now;
    var remaining = wait - (now - previous);
    context = this;
    args = arguments;
    if (remaining <= 0 || remaining > wait) {
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      previous = now;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    } else if (!timeout && options.trailing !== false) {
      timeout = setTimeout(later, remaining);
    }
    return result;
  };
};

export default {
  shortCode: shortCode,
  edgeDistributions: edgeDistributions,
  buildDistributions: buildDistributions,
  transposePoint: transposePoint,
  offsetPoint: offsetPoint,
  offsetPoints: offsetPoints,
  clipPath: clipPath,
  d3CurvedLine: d3CurvedLine,
  debounce: debounce,
  throttle: throttle
};
