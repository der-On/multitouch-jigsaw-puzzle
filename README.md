# Multitouch Jigsaw Puzzle

A multitouch capable jigsaw puzzle game, that turns any image URI into a puzzle.

Uses local storage for persistance.
